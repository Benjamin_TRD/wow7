CREATE TABLE IF NOT EXISTS pet_types (
pet_types_id INT NULL,
pet_types_key VARCHAR(10) NULL,
pet_types_name VARCHAR(13) NULL,
pet_types_strong_against_id INT NULL,
pet_types_type_ability_id INT NULL,
pet_types_weak_against_id INT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO pet_types VALUES
(0,"humanoid","Humanoïde",1,238,7),
(1,"dragonkin","Draconien",5,245,3),
(2,"flying","Aérien",8,239,1),
(3,"undead","Mort-vivant",0,242,8),
(4,"critter","Bestiole",3,236,0),
(5,"magical","Magique",2,243,9),
(6,"elemental","Élémentaire",9,241,4),
(7,"beast","Bête",4,237,2),
(8,"water","Aquatique",6,240,5),
(9,"mechanical","Machine",7,244,6);